# Simple Flask App

## Objetivo

Uma aplicação bem simples para exemplo feita em Flask e Python 3.
Essa aplicação vai exibir a foto do dia da NASA.

## Como executar esse código ?

### Instalar dependencias

Para instalar todas as dependencias recomendamos usar o virtualenv e o pip.

```sh
# Install dependencies from Pipfile
pipenv install
```

### Rodar servidor local

Use o seguinte comando

```sh
python app.py
```

Você deve ter uma saida como:

 * Serving Flask app 'app' (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
```
```

Ai é só acessar o link no navegador.


## NASA API

Exemplo de chamada com demo key

`curl -X GET https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY`

Exemplo de resposta:

```json
{
  copyright: "Michael Teoh",
  date: "2021-07-02",
  explanation: "Awash in a sea of incandescent plasma and anchored in strong magnetic fields, sunspots are planet-sized dark islands in the solar photosphere, the bright surface of the Sun. Found in solar active regions, sunspots look dark only because they are slightly cooler though, with temperatures of about 4,000 kelvins compared to 6,000 kelvins for the surrounding solar surface. These sunspots lie in active region AR2835. The largest active region now crossing the Sun, AR2835 is captured in this sharp telescopic close-up from July 1 in a field of view that spans about 150,000 kilometers or over ten Earth diameters. With powerful magnetic fields, solar active regions are often responsible for solar flares and coronal mass ejections, storms which affect space weather near planet Earth.",
  hdurl: "https://apod.nasa.gov/apod/image/2107/AR2835_20210701_W2x.jpg",
  media_type: "image",
  service_version: "v1",
  title: "AR2835: Islands in the Photosphere",
  url: "https://apod.nasa.gov/apod/image/2107/AR2835_20210701_W2x1024.jpg"
}
```
