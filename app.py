import os
import json
import requests

from flask import Flask
from flask import render_template

API_KEY = os.environ.get("NASA_API_KEY", "DEMO_KEY")
API_ENDPOINT = "https://api.nasa.gov/planetary/apod?api_key="+API_KEY

app = Flask(__name__)

@app.route('/')
def index_page():
    return render_template('index.html', data = fetch_data())

def fetch_data():
    response = requests.get(API_ENDPOINT)
    body = response.json()
    return body


if __name__ == '__main__':
    app.run(host='0.0.0.0')
