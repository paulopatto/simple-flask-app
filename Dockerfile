FROM python:3.8-alpine


RUN mkdir -p /var/www/simple-flask-app
COPY . /var/www/simple-flask-app
WORKDIR /var/www/simple-flask-app

# RUN python3 -m venv venv
# RUN source venv/bin/activate

RUN pip install pipenv
RUN pipenv install --system --deploy

EXPOSE 5000
ENTRYPOINT ["pipenv", "run", "python3", "app.py"]
