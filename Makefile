app_name = "paulopatto/simple-flask-app"

build:
	@docker build -t $(app_name) .

run:
	docker run --detach -p 5000:5000 $(app_name)

deploy:
	@docker push $(app_name):latest

kill:
	@echo 'Killing container...'
	@docker ps | grep $(app_name) | awk '{print $$1}' | xargs docker stop
